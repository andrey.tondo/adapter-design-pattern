package main.java.adapter;

import main.java.adapter.imagem.enums.FormatoImagem;

import java.util.*;

public class Main {
    public static void main(String[] args) {

        exemploUm();
        System.out.println("=================");
        exemploDois("José", "Matheus");
    }

    public static void exemploUm() {
        Galeria galeria = new Galeria();

        galeria.exibir("céu azul", FormatoImagem.PNG);
        galeria.exibir("céu azul", FormatoImagem.JPG);
        galeria.exibir("céu azul", FormatoImagem.GIF);
    }

    public static void exemploDois(String... nomes) {
        String[] nomesArray = nomes;

        List<String> nomesLista = Arrays.asList(nomesArray);
        Enumeration<String> nomesIterator = Collections.enumeration(nomesLista);

        System.out.println(Arrays.toString(nomesArray));
        System.out.println(nomesLista);

        while (nomesIterator.hasMoreElements()) {
            System.out.print(nomesIterator.nextElement() + " ");
        }
    }

}
