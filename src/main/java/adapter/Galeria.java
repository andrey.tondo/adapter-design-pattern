package main.java.adapter;

import main.java.adapter.imagem.enums.FormatoImagem;
import main.java.adapter.imagem.visualizador.VisualizadorAdapter;
import main.java.adapter.imagem.visualizador.VisualizadorImagem;

import java.util.Arrays;

public class Galeria implements VisualizadorImagem {

    private VisualizadorAdapter visualizadorAdapter;

    @Override
    public void exibir(String nomeArquivo, FormatoImagem formato) {
        if (FormatoImagem.JPG.equals(formato)) {
            System.out.println("Imagem sendo exibida: " + nomeArquivo + ".jpg");

        } else if(Arrays.asList(FormatoImagem.PNG, FormatoImagem.GIF).contains(formato)) {
            visualizadorAdapter = new VisualizadorAdapter(formato);
            visualizadorAdapter.exibir(nomeArquivo, formato);
        }
    }

}
