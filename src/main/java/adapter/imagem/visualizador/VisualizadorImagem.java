package main.java.adapter.imagem.visualizador;

import main.java.adapter.imagem.enums.FormatoImagem;

public interface VisualizadorImagem {

    void exibir(String nomeArquivo, FormatoImagem formato);
}
