package main.java.adapter.imagem.visualizador;

import main.java.adapter.imagem.enums.FormatoImagem;
import main.java.adapter.imagem.visualizadoravancado.VisualizadorImagemAvancado;
import main.java.adapter.imagem.visualizadoravancado.VisualizadorGif;
import main.java.adapter.imagem.visualizadoravancado.VisualizadorPng;

public class VisualizadorAdapter implements VisualizadorImagem {

    private VisualizadorImagemAvancado visualizadorAvancado;

    @Override
    public void exibir(String nomeArquivo, FormatoImagem formato) {
        if (FormatoImagem.PNG.equals(formato)) {
            visualizadorAvancado.mostrarPng(nomeArquivo);
        } else if (FormatoImagem.GIF.equals(formato)) {
            visualizadorAvancado.mostrarGif(nomeArquivo);
        }
    }

    public VisualizadorAdapter(FormatoImagem formato) {

        if (FormatoImagem.PNG.equals(formato)) {
            visualizadorAvancado = new VisualizadorPng();
        } else if (FormatoImagem.GIF.equals(formato)) {
            visualizadorAvancado = new VisualizadorGif();
        }
    }

}
