package main.java.adapter.imagem.visualizadoravancado;

public class VisualizadorPng implements VisualizadorImagemAvancado {

    @Override
    public void mostrarPng(String nomeArquivo) {
        System.out.println("Imagem sendo exibida: " + nomeArquivo + ".png");
    }

    @Override
    public void mostrarGif(String nomeArquivo) {
        // nao implementar
    }

}
