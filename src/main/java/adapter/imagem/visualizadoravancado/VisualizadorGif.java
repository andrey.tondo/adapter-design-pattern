package main.java.adapter.imagem.visualizadoravancado;

public class VisualizadorGif implements VisualizadorImagemAvancado {

    @Override
    public void mostrarGif(String nomeArquivo) {
        System.out.println("Imagem sendo exibida: " + nomeArquivo + ".gif");
    }

    @Override
    public void mostrarPng(String nomeArquivo) {
        // nao implementar
    }

}
