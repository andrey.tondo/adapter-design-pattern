package main.java.adapter.imagem.visualizadoravancado;

public interface VisualizadorImagemAvancado {

    void mostrarPng(String nomeArquivo);
    void mostrarGif(String nomeArquivo);
}
