package main.java.adapter.imagem.enums;

public enum FormatoImagem {

    GIF(".gif"),
    JPG(".jpg"),
    PNG(".png");

    private final String descricao;

    FormatoImagem(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

}
